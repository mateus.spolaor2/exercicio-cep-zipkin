package br.com.zipkin.usuarioCep.usuarios.repositories;

import br.com.zipkin.usuarioCep.usuarios.models.Cep;
import br.com.zipkin.usuarioCep.usuarios.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Optional<Usuario> findByCep(Cep cep);

}
