package br.com.zipkin.usuarioCep.usuarios.clients;

import br.com.zipkin.usuarioCep.usuarios.models.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep")
public interface CepClient {
    @GetMapping("/cep/{cep}/")
    Cep getCep(@PathVariable String cep);
}

