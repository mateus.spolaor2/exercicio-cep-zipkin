package br.com.zipkin.usuarioCep.usuarios.DTOs;

public class UsuarioDTO {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public UsuarioDTO() {
    }

    public UsuarioDTO(String nome) {
        this.nome = nome;
    }
}
