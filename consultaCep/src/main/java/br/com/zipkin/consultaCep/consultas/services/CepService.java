package br.com.zipkin.consultaCep.consultas.services;

import br.com.zipkin.consultaCep.consultas.clients.CepClient;
import br.com.zipkin.consultaCep.consultas.models.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    CepClient cepClient;

    @NewSpan(name = "viacep-service")
    public Cep getByCep(@SpanTag("cep") String cep) {
        return cepClient.getCep(cep);
    }
}
